<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-09-15 15:18:51
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2022-10-23 22:20:09
 */

namespace Ldy\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Ldy\Facade\ClientConfig;

class LdyAdminGenerateConfig extends Command{

    protected static $defaultName = 'ldy-admin:clientConfig';
    protected static $defaultDescription = '生成客户端配置文件';

    /**
     * @return void
     */
    protected function configure()
    {
        // $this->addArgument('name', InputArgument::REQUIRED, '输入控制器路径');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        ClientConfig::generateConfig();

        $output->writeln('生成前端配置文件成功!');
        
        return self::SUCCESS;
    }
}