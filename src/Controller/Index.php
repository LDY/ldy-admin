<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-08-08 16:40:42
 */
namespace Ldy\Controller;

use Ldy\Controller\BaseAdmin;
use support\Request;
use Casbin\WebmanPermission\Permission;
use Ldy\Models\SysMenu;
use think\facade\Db;
use Ldy\Facade\ClientConfig;
use Ldy\Facade\PhpWord;
use Ldy\Models\SysConfig;
use Think\facade\Cache;

class Index extends BaseAdmin{

    /**
     * 无需登录的方法及鉴权
     * @var array
     */
    protected $noNeedLogin = ['index'];

    public function index(){

        return json(SysConfig::getItem('sys.name'));
    }

}