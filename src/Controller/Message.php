<?php
/*
 * @Date: 2022-10-29 12:51:45
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2022-11-11 22:54:31
 */

namespace Ldy\Controller;

use Ldy\Controller\BaseAdmin;

class Message extends BaseAdmin{
 
    protected $noNeedAuth = ['index'];
    
    public function index()
    {
        $data = [];
        //扩展网站信息
        if(method_exists("\\app\\admin\\controller\\SysMessage",'index')){
            $data = (new \app\admin\controller\SysMessage)->index();
        }

        return $this->successJson($data);
        
    }
}