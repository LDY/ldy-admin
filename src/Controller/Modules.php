<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-08-13 15:51:10
 * @LastEditors: LDY dong2406@126.com
 * @LastEditTime: 2022-09-27 23:05:08
 */
namespace Ldy\Controller;

use Ldy\Controller\BaseAdmin;
use Ldy\Models\SysModules as ModulesModel;
use Ldy\Lib\Form;
use Ldy\Lib\Grid;
// use support\Request;
// use think\facade\Cache;

class Modules extends BaseAdmin{

    protected $title = "模块管理";

    protected $noNeedAuth = [];

    /**
     * 列表
     */
    protected function grid(){

        $grid = new Grid(new ModulesModel());

        $grid->border(false);

        $grid->quickSearch(['id'=>"ID",'name'=>'模块别名']);

        $grid->column("id", 'Id')->width(100);
        
        $grid->column("name","模块名称");
        
        $grid->column("label","模块别名");
        
        $grid->column("versions", "版本号")->tag('success')->attrs(["effect"=>"dark"]);
        $grid->column("state", "状态")->using([1=>'开启',0=>'关闭'])->select();
        $grid->column("rand", "排序");
        $grid->column("update_time", "更新时间");

        return $grid;
    }

    /**
     * 创建表单
     */
    protected function form(){

        $form = new Form(new ModulesModel());

        $form->setOption(["labelPosition"=>"left"], "form");
        $form->text('name','模块名称')->required()->unique()->editDisabled();
        
        $form->text('label','系统标识')
        ->addValidate(["required" => true, "message"=>"数据不能为空！"])
        ->addValidate(["min"=> 2, "message"=>'不能少于两个文字',])
        ->addValidate(["max"=> 20, "message"=>"不能大于20个文字"]);
        
        $form->text('versions','模块版本')->required()->value('1.0.0');
        $form->text('relation','关联模块');
        $form->number('rand','模块排序')->required()->value(1)->addValidate(["type"=>"number", "message"=>"请填写整数"]);
        $form->textarea('desc','模块描述');


        // $form->saving(function(Form $form){
        //     //查询别名是否重复
        //     $module = $form->model->where("name", $form->model->name)->select();
        //     if(!$module->isEmpty()) return "模块名称已存在！";
        // });

        return $form;
    }
}