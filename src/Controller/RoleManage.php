<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-09-19 23:00:16
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2023-03-04 16:34:28
 */

namespace Ldy\Controller;

use Ldy\Controller\BaseAdmin;
use Ldy\Lib\Form;
use Ldy\Lib\Grid;
use Ldy\Lib\Show;
use Ldy\Models\SysRole;
use Ldy\Models\SysMenu;
use Ldy\Models\SysDepartment;

class RoleManage extends BaseAdmin{

    protected $title = "角色";

    protected $dataAuth = [0=>"全部可见", 1=>"本人可见", 2=>"所在部门可见", 3=>"所在部门及子级可见", 4=>"选择的部门可见"];

     /**
     * 列表
     */
    protected function grid(){

        $grid = new Grid(new SysRole());

        $grid->model()->alias('r')
        ->field('r.*,GROUP_CONCAT(m.menu_id) AS menu')
        ->leftJoin('sys_role_menu m','m.role_id=r.id')
        ->group('r.id');

        $grid->column("id", '#')->width(100)->sortable();
        $grid->column("name","角色名称");
        $grid->column("label","角色别名");

        $grid->column("status", "状态")->using([1=>'开启',0=>'关闭']);
        $grid->column("rand", "排序");

        $grid->column("bz", "备注");
        $grid->column("update_time", "更新时间");

        return $grid;
    }

    protected function detail(int $id)
    {
        $show = new Show(SysRole::find($id));

        $show->field("id", 'ID');
        $show->field("name","角色名称");
        $show->field("label","角色别名");

        $show->field("status", "状态")->using([1=>'开启',0=>'关闭']);
        $show->field("rand", "排序");

        $show->field("bz", "备注");
        $show->field("update_time", "更新时间");

        return $show;
    }

    protected function form(){

        $form = new Form(new SysRole(), function($model, $id){
            return $model->alias('r')
            ->field('r.*,GROUP_CONCAT(m.menu_id) AS menu')
            ->leftJoin('sys_role_menu m','m.role_id=r.id')
            ->group('r.id')->find($id);
        });

        // $form->setOption(['labelPosition'=>'left'], 'form');

        $form->group('基本信息',function(Form $form){
            // $form->hidden('id','ID');
            $form->text('name','角色名称')->required();
            $form->text('label', '角色别名');
            $form->number('rand', '排序')->value(1)->btnRight();
            $form->switch('status', '是否启用')->value(1);
            $form->textarea('bz', '备注');
        });
        
        $form->group('菜单权限',function(Form $form){
            $menuModel = new SysMenu();
            $trees = $menuModel->trees(['id'=>'id','title'=>'meta.title','rand'=>'rand']);
            $publicMenuIds = $menuModel->getPublicMenuIds();
            $form->trees('menu', $trees)->showCheckbox()->checkStrictly()->defaultExpandedKeys()->value($publicMenuIds);
        });

        $form->group('数据权限', function(Form $form){
            $form->select('data_auth','规则类型')->options($this->dataAuth)
            ->value(0)
            ->control(4, function(Form $form){
                $trees = (new SysDepartment())->trees(['id'=>'id','title'=>'name','rand'=>'rand']);
                $form->trees('dept_ids','选择部门', $trees)->showCheckbox();
            });
        });

        return $form;
    }
}