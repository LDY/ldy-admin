<?php
/*
 * @Date: 2022-10-06 22:40:35
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2022-10-06 22:41:11
 */

namespace Ldy\Facade;

 //使用thinkphp 静态代理
use think\Facade;

class PhpWord extends Facade{

    protected static function getFacadeClass()
    {
        return 'Ldy\Lib\PhpWord';
    }
}
