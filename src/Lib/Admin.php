<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-06-12 15:01:45
 */

namespace Ldy\Lib;

use Webman\Route;
use Ldy\Controller;

class Admin
{
    /**
     * 版本号
    * @var string
    */
    const version = '1.2.8';


    public function route(){
        $config = config('plugin.ldy.admin.app.route');

        Route::group( '/'.$config['prefix'], function(){
        
            Route::get('[/]', "Ldy\\Controller\\Index@index");

            //登录接口
            Route::post('/token', [Controller\Account::class, 'login']);
            //退出
            Route::get('/logout', [Controller\Account::class, 'logout']);
            //获取账户菜单
            Route::get('/system/menu/my', [Controller\Account::class, 'myMenu']);
            //获取菜单列
            Route::get('/system/menu/list', [Controller\System::class, 'menuList']); 
            
            //文件上传接口
            Route::post('/files/upload', [Controller\BaseAdmin::class, 'upload']);
             // 上传头像
            Route::post('/avatar/upload', [Controller\Account::class, 'avatarUpload']);
			
            //系统设置
			Route::resource('/system/setting', Controller\Setting::class, ['create','store']);

            //模块管理
            Route::resource('/system/modules', Controller\Modules::class);

            //部门管理
            Route::resource('/system/auth/department', Controller\Department::class);

            //API管理
            Route::resource('/system/auth/api_manage', Controller\ApiManage::class);

            //菜单管理
            Route::resource('/system/auth/menu', Controller\Menu::class);

             //角色管理
            Route::resource('/system/auth/role', Controller\RoleManage::class);

            //用户关联
            Route::resource('/system/auth/admin', Controller\AdminManage::class);

            //获取数据表字段信息
            Route::get('/system/get_table_fields', "Ldy\\Controller\\System@getTableFields");

            //账号信息
            Route::get('/system/account_info', [Controller\Account::class, 'accountInfo']);
            //账号信息修改
            Route::post('/system/account_info', [Controller\Account::class, 'accountInfo']);
            //密码修改
            Route::post('/system/update_password',[Controller\Account::class, 'updatePassword']);

            //系统操作日志
            Route::get('/system/logs', [Controller\Logs::class, 'index']);

            Route::get('/system/logs/{id}', [Controller\Logs::class, 'show']);

            Route::get('/system/msg', [Controller\Message::class, 'index']);
            
            Route::get('/system/long_task/restore/{id}',[Controller\BackupData::class, 'restore']);
            Route::post('/system/long_task/backup',[Controller\BackupData::class, 'store']);
            Route::resource('/system/backup',Controller\BackupData::class,["index","create","show","destroy"]);
            

        })->middleware([
            \Ldy\Middleware\Logs::class,
            \Ldy\Middleware\AccessControl::class
        ]);
        

        Route::fallback(function(){
            return json(['code' => 404, 'msg' => '404 not found']);
        });
    }


    public function checkAccess(string $controller, string $action, int &$code = 0, string &$msg = ''){
        // 获取控制器鉴权信息
        $class = new \ReflectionClass($controller);
        $properties = $class->getDefaultProperties();
        $noNeedLogin = $properties['noNeedLogin'] ?? [];
        $noNeedAuth = $properties['noNeedAuth'] ?? [];

        // 不需要登录
        if (in_array($action, $noNeedLogin)) {
            return true;
        }

        // 获取登录信息
        $admin = admin_info();
        if (!$admin) {
            $msg = '请登录';
            // 401是未登录固定的返回码
            $code = 401;
            return false;
        }

        // 不需要鉴权
        if (in_array($action, $noNeedAuth)) {
            return true;
        }

        // 当前管理员无角色
        $roles = $admin['roles'];
        if (empty($roles)) {
            $msg = '无权限';
            $code = 2;
            return false;
        }
    }
}