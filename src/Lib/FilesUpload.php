<?php
/**
 * 重新 文件上传类， 支持动态配置
 * @Date: 2024-06-09 20:52:22
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-06-09 20:54:44
 */

namespace Ldy\Lib;
use Tinywan\Storage\Storage;
use Tinywan\Storage\Exception\StorageException;

class FilesUpload extends Storage{
     /**
     * @desc: 方法描述
     *
     * @author Tinywan(ShaoBo Wan)
     */
    public static function init(string $storage = null, Array $config = [], bool $_is_file_upload = true)
    {
        $defConfig = config('plugin.tinywan.storage.app.storage');
        $storage = $storage ?: $defConfig['default'];
        if (!isset($defConfig[$storage]) || empty($defConfig[$storage]['adapter'])) {
            throw new StorageException('对应的adapter不存在');
        }

        $conf = array_merge($defConfig[$storage], $config , ['_is_file_upload' => $_is_file_upload]);

        static::$adapter = new $defConfig[$storage]['adapter']($conf);

        return static::$adapter;
    }
}