<?php

namespace Ldy\Lib;

use PhpOffice\PhpWord\PhpWord as PhpOfficeWord;
use PhpOffice\PhpWord\Element\Section as PhpOfficePhpWordElementSection;
use PhpOFFICE\PhpWord\Element\TextRun as PhpOfficePhpWordElementTextRun;
use PhpOffice\PhpWord\Element\Table as PhpOfficePhpWordElementTable;
use PhpOffice\PhpWord\Element\Text as PhpOfficePhpWordElementText;
use PhpOffice\PhpWord\Element\Image as PhpOfficePhpWordElementImage;
use PhpOffice\PhpWord\Element\Row as PhpOfficePhpWordElementRow;
use PhpOffice\PhpWord\Element\Cell as PhpOfficePhpWordElementCell;
use PhpOffice\PhpWord\IOFactory;

class PhpWord{

    protected $loadWord;

    public function load(String $path){
        $this->loadWord = IOFactory::load($path);
        return $this;
    }

    public function getNodeContent()
    {
        $return = [];
        //分解部分
        foreach ($this->loadWord->getSections() as $section)
        {
            if ($section instanceof PhpOfficePhpWordElementSection) {
                //分解元素
                foreach ($section->getElements() as $element)
                {
                    //文本元素
                    if ($element instanceof PhpOfficePhpWordElementTextRun) {
                        $text = '';
                        foreach ($element->getElements() as $ele) {
                            $text .= $this->getTextNode($ele);
                        }
                        $return[] = $text;
                    }else if ($element instanceof PhpOfficePhpWordElementTable) {
                        //表格元素
                        foreach ($element->getRows() as $ele)
                        {
                            $return[] = $this->getTableNode($ele);
                        }
                    }
                }
            }
        }
        return $return;
    }

     /**
     * 获取文档节点内容
     * @param $node
     * @return string
     */
    protected function getTextNode($node)
    {
        $return = '';
        //处理文本
        if ($node instanceof PhpOfficePhpWordElementText)
        {
            $return .= $node->getText();
        }
        //处理图片
        else if ($node instanceof PhpOfficePhpWordElementImage)
        {
            $return .= $this->pic2text($node);
        }
        //处理文本元素
        else if ($node instanceof PhpOfficePhpWordElementTextRun) {
            foreach ($node->getElements() as $ele) {
                $return .= $this->getTextNode($ele);
            }
        }
        return $return;
    }

    /**
     * 获取表格节点内容
     * @param $node
     * @return string
     */
    protected function getTableNode($node)
    {
        $return = [];
        //处理行
        if ($node instanceof PhpOfficePhpWordElementRow) {
            foreach ($node->getCells() as $ele){
                $return[] = $this->getTableNode($ele);
            }
        }else if($node instanceof PhpOfficePhpWordElementCell) {
        //处理列
            foreach ($node->getElements() as $ele){
                $return[] =  $this->getTextNode($ele);//preg_replace("/\s| /","",$this->getTextNode($ele));
            }
        }
        return $return;
    }

    /**
     * 处理word文档中base64格式图片
     * @param $node
     * @return string
     */
    protected function pic2text($node)
    {
        //获取图片编码
        $imageData = $node->getImageStringData(true);
        //添加图片html显示标头
        $imageData = 'data:' . $node->getImageType() . ';base64,' . $imageData;
        $return = '<img src="'.$imageData.'">';
        return $return;
    }
    /**
     * 处理word文档中base64格式图片
     * @param $node
     * @return string
     */
    protected function pic2file($node)
    {
        //图片地址(一般为word文档地址+在word中的锚点位置)
        $imageSrc  = 'images/' . ($node->getSource()) . '.' . $node->getImageExtension();
        $imageData = $node->getImageStringData(true);
        //将图片保存在本地
        file_put_contents($imageSrc, base64_decode($imageData));
        return $imageSrc;
    }
}