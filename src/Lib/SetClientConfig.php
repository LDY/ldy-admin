<?php
/*
 * @Date: 2022-10-23 15:20:42
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-09-03 15:45:28
 */

namespace Ldy\Lib;

use Ldy\Models\SysConfig;

class SetClientConfig{

    // private $fileContent = '';

    private $clientConfigfileName = 'config.js';

    private $filePath = '';

    private $defConfig = [
        "APP_NAME" => '',
        // "API_URL" => '',
        "EXTEND_MODULES"=>[]
    ];


    public function generateConfig(){

        $this->get();

        $str = 'const APP_CONFIG ='. json_encode($this->defConfig);
        file_put_contents($this->filePath, $str);
        return $this;
    }

    public function get(){
         $this->filePath = public_path().DIRECTORY_SEPARATOR.$this->clientConfigfileName;

        $dbConfig = SysConfig::select();

        $dbConf = [];
        foreach($dbConfig as $item){
            $type = $item->type;
            $dbConf[$type] = $item->fields;
        }

        $sysConfig = $dbConf['sys'] ??[]; //SysConfig::where('type', 'sys')->find();

        $appName = config('plugin.ldy.admin.app.title','琪笙');

        if($sysConfig && $sysConfig->fields && isset($sysConfig->fields->name)) $appName = $sysConfig->fields->name;

        $this->defConfig['APP_NAME'] = $appName;

        $this->defConfig['EXTEND_MODULES'] = config('plugin.ldy.admin.app.modules', []);

        $this->defConfig['APP_URL'] = config('plugin.ldy.admin.app.api_url');

        $apiUrl = config('plugin.ldy.admin.app.api_url', '');
        if($sysConfig && $sysConfig->fields && isset($sysConfig->fields->api_url) && !empty($sysConfig->fields->api_url)) $apiUrl = $sysConfig->fields->api_url;
        if($apiUrl) $this->defConfig['API_URL'] = $apiUrl;

        $extendSetting = config('plugin.ldy.admin.app.extend.setting');
        if($extendSetting) $this->defConfig = $extendSetting::toClientConfig($this->defConfig, $dbConf) ?? $this->defConfig;

        return $this->defConfig;
    }
}