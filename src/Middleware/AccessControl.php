<?php
/*
 * @Date: 2022-09-27 11:23:42
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-05-29 12:52:40
 */
namespace Ldy\Middleware;

use Webman\MiddlewareInterface;
use Webman\Http\Response;
use Webman\Http\Request;
use Ldy\Lib\Auth;

class AccessControl implements MiddlewareInterface{

    public function process(Request $request, callable $handler) : Response
    {
        $code = 0;
        $msg = '';
        if(!Auth::checkAccount($request, $code, $msg)){
            if($code === 401) session_id_refresh($request);
            return json(['code' => $code, 'msg' => $msg]);
        }
        return $handler($request);
    }
}