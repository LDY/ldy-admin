<?php
/*
 * @Date: 2022-09-27 11:23:42
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-04-27 13:14:33
 */
namespace Ldy\Middleware;

use Webman\MiddlewareInterface;
use Webman\Http\Response;
use Webman\Http\Request;
use Ldy\Models\SysLogs;

class Logs implements MiddlewareInterface{
    
    public function process(Request $request, callable $handler) : Response
    {
        $controller = $request->controller;
        $action = $request->action;
        $method = $request->method();
        $path = $request->path();
        $ip = $request->getRealIp(true);
        $admin = session('admin');
        $reqData = ['post'=>$request->post(),'header' => $request->header(), 'get'=>$request->get()];

        $noRecordLog = $request->header('QsNoRecordLog');

        $response = $handler($request);
        //不记录日志
        if($noRecordLog) return $response;

        // 获取控制器鉴权信息
        $class = new \ReflectionClass($controller);
        $properties = $class->getDefaultProperties();
       
        $classMethod = $class->getMethod($action);

        $doc = $classMethod->getDocComment();

        preg_match('/\{(.*)\}/', $doc, $match);
        $jsonStr = $match ? $match[0]:[];
        $json = empty($jsonStr) ? []:json_decode($jsonStr, true);

        $logs = $json ?? [];

        $status = $response->getStatusCode();
        $res = json_decode($response->rawBody(), true);

        if($properties['isRecordLogs'] && !empty($logs) && $status == 200 && !empty($res)){

            $code = $res['code'];
            if(isset($res['msg'])) $reqData['response'] = $res['msg'];

            if(empty($admin)) $admin = session('admin');
            $data = [];

            $logTitle = $action;
            if(isset($logs[$method])) $logTitle = is_array($logs[$method]) ?  $logs[$method]['title']:$logs[$method];
            
            $info = isset($res['data']) && !empty($res['data']) && is_array($res['data']) ? array_merge($res['data'],$reqData['post']):$reqData['post'];
            
            if($properties['title']) $logTitle = preg_replace('/{title}/', $properties['title'], $logTitle);

            $logTitle = preg_replace_callback('/\{(.*?)\}/', function($match) use ($info){
                if(count($match)< 1 ) return '';
                $key = $match[1];
                $val = isset($info[$key]) ? $info[$key]:'';
                return $val;
            }, $logTitle);

            $data['title'] = $logTitle;
            $data['url'] = $path;
            $data['admin_id'] = empty($admin['id']) ? 0:$admin['id'];
            $data['ip'] = $ip;
            $data['method'] = $method;
            $data['data'] = $reqData;
            $data['code'] = $code;
            
            $data['name'] = isset($admin['nickname']) ?  $admin['nickname']: '-';

            SysLogs::create($data);
        }


        return $response;
    }
}