<?php
/*
 * Thinkphp orm
 * @Author: LDY dong2406@126.com
 * @Date: 2022-08-12 10:46:55
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2022-10-08 12:12:51
 */
namespace Ldy;

use think\Model as BaseModel;
use think\Validate;

class Model extends BaseModel{

    /**
     * 模型更新白名单
     *
     * @var array
     */
    public $updateWhiteList = [];
    /**
     * 验证规则
     * 使用thinkphp Validate 验证
     * @var array
     */
    protected $rule = [];

    /**
     * 验证规则错误信息
     *
     * @var array
     */
    protected $ruleMessage = [];


    public function getRule(){
        print_r($this->rule);
        print_r($this->ruleMessage);
    }

    /**
     * 获取隐藏字段
     *
     * @return void
     */
    public function getHiddenFields(){
        return empty($this->hidden) ? []:$this->hidden;
    }

    /**
     * 设置验证规则，如何数据模型存在规则，则数据模型的优先
     *
     * @return $this
     */
    public function setRule(string $name, $rule){
        if(empty($this->rule[$name])) $this->rule[$name] = $rule;
        return $this;
    }

    /**
     * 验证规则错误信息
     *
     * @param string $name
     * @param string $message
     * @return $this
     */
    public function setRuleMessage(string $name, string $message){
        $this->ruleMessage[$name] = $message;
        return $this;
    }
    /**
     * 获取表字段信息列
     * 可声明 fieldsLabel 属性覆盖别名。
     * 可声明 fieldsLabelRand 属性 true ：按fieldsLabel数组顺序， false：按数据字段顺序
     * 格式数组 [字段名=>别名]
     * 
     * @return array
     */
    public function getTableFieldsList(): array
    {
        $fields = $this->getFields();

        if(empty($this->fieldsLabel)) return $fields;


        $tmp = [];
        foreach($fields as $k => $field) $tmp[$field['name']] = $field;
        
        $flabel = [];
        foreach($this->fieldsLabel as $k => $label){
            if(is_array($label)){
                $tmp[$k] = array_merge($tmp[$k], $label);
            }else{
                $tmp[$k]['comment'] = $label;
            }
            
            $flabel[$k] = $tmp[$k];
        }

        if($this->fieldsLabelRand === true){
            //根据fieldsLabel 数据顺序
            $flabel = array_merge($flabel, $tmp);
        }else{
             //根据数据库 字段数据顺序
            $flabel = array_merge($tmp, $flabel);
        }

        $res = [];
        foreach($flabel as $item) $res[] = $item;
        
        return $res;
    }

    /**
     * 验证POST数据
     *
     * @param Array $data
     * @return void
     */
    public function validateData(Array $data)
    {
        $validate = new Validate();
        $validate->message($this->ruleMessage);
        if(!$validate->check($data, $this->rule)) return $validate->getError();

        return true;
    }

    public static function relTbleUpdate(Int $id, Array $data){
        
    }

    

    /**
     * 二维数组排序
     *
     * @param [type] $arr
     * @param [type] $keys
     * @param string $type
     * @return Array
     */
    protected function array_sort($arr,$keys,$type='asc'){

		$keysvalue = $new_array = array();
		
		foreach($arr as $k=>$v) $keysvalue[$k] = $v[$keys];
		
		if($type == 'asc'){
			asort($keysvalue);
		}else{
			arsort($keysvalue);
		}
		
		reset($keysvalue);
		
		foreach ($keysvalue as $k=>$v) $new_array[] = $arr[$k];
		
		return $new_array;
		
	}
}