<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-08-08 17:19:07
 */
namespace Ldy\Models;

use Ldy\Model;

class SysLogs extends Model{

    protected $schema = [
        "id"=>"bigint",
        "title" => 'varchar',
        "url" => 'varchar',
        'method'=>'varchar',
        'admin_id' => 'int',
        'ip' => 'varchar',
        'create_time' => 'int',
        'code' => 'varchar',
        'data' => 'json',
        'name' => 'varchar'
    ];
}