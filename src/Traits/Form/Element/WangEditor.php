<?php
/*
 * @Date: 2022-09-23 13:40:40
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2024-06-21 12:43:53
 */

namespace Ldy\Traits\Form\Element;


trait WangEditor{
    
    protected $toolbarConfig = [];
    protected $editorConfig = [];

    public function setEditorStyle(String $style){
        
        return $this->props(["editorStyle"=>$style]);
    }
    /**
     * 工具配置
     *
     * @param Array $config
     * @return $this
     */
    public function setToolbarConfig(Array $config){
        $this->toolbarConfig = array_merge($this->toolbarConfig, $config);
        return $this->props(["toolbarConfig"=>$this->toolbarConfig]);
    }

    /**
     * 工具 排除
     *  
     * @param Array $keys
     * @return $this
     */
    public function setEditToolbarExcludeKeys(Array $keys){
        return $this->setToolbarConfig(["excludeKeys"=>$keys]);
    }

    //编辑器配置
    public function setEditorConfig(Array $config){
        $this->editorConfig = array_merge($this->editorConfig, $config);
        return $this->props(["editorConfig"=>$this->editorConfig]);
    }

    //图片上传配置
    public function setMenuConfUploadImage(Array $config){
        $this->editorConfig = array_merge($this->editorConfig, ["MENU_CONF"=>["uploadImage"=>$config]]);
        return $this->setEditorConfig($this->editorConfig);
    }

    //视频上传设置
    public function setMenuConfUploadVideo(Array $config){
        $this->editorConfig = array_merge($this->editorConfig, ["MENU_CONF"=>["uploadVideo"=>$config]]);
        return $this->setEditorConfig($this->editorConfig);
    }
    
}