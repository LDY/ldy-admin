<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-09-04 17:05:21
 * @LastEditors: LDY dong2406@126.com
 * @LastEditTime: 2022-09-26 16:05:43
 */

namespace Ldy\Traits\Form\Element;

trait trees{

    /**
     * 节点是否可被选择
     *
     * @return $this
     */
    public function showCheckbox(){
        $this->__updateRule(["props"=>["showCheckbox"=>true]]);
        return $this;
    }

    public function emit(){
        $this->__updateRule(["emit"=>["check"],"emitPrefix"=>'qs-tree']);
        return $this;
    }
}
