<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-06-04 17:23:38
 * @LastEditors: LDY dong2406@126.com
 * @LastEditTime: 2022-08-28 10:46:00
 */

return [
    support\bootstrap\Session::class,
    // support\bootstrap\LaravelDb::class,
    Webman\ThinkOrm\ThinkOrm::class,
    Webman\ThinkCache\ThinkCache::class,
];
