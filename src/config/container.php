<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-06-04 17:23:38
 * @LastEditors: LDY dong2406@126.com
 * @LastEditTime: 2022-08-29 08:41:40
 */

//return new Webman\Container;

$builder = new \DI\ContainerBuilder();
$builder->addDefinitions(config('dependence', []));
$builder->useAutowiring(true);
return $builder->build();
