<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-08-28 17:29:28
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2022-10-01 11:53:09
 */

return [
    'default' => 'basic',
    // 基础配置
    'basic' => [
        // 策略模型Model设置
        'model' => [
            'config_type' => 'file',
            'config_file_path' => config_path() . '/plugin/casbin/webman-permission/rbac-model.conf',
            'config_text' => '',
        ],
        // 适配器
        'adapter' => Casbin\WebmanPermission\Adapter\DatabaseAdapter::class, // ThinkORM 适配器
        // 'adapter' => Casbin\WebmanPermission\Adapter\LaravelDatabaseAdapter::class, // Laravel 适配器
        // 数据库设置
        'database' => [
            'connection' => '',
            'rules_table' => env('DB_PREFIX','').'sys_casbin_rule',
            'rules_name' => null
        ],
    ],
    // 其他扩展配置，只需要按照基础配置一样，复制一份，指定相关策略模型和适配器即可
];