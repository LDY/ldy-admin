<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-06-11 16:50:22
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2022-12-01 15:57:23
 */
return [
    'enable' => true,
    'title' => env('APP_NAME','琪笙'),
    'route' => [
        'prefix' => env('ADMIN_ROUTE_PREFIX', 'admin'),
        'middleware' => ['web', 'admin'],
    ],
    //casbin权限 角色字段前缀
    'casbin_role_field_prefix'=>'role_',
    //超级管理员账号名
    'root_name'=> 'admin',

    'modules' => [
        "module-system"=>"/modules/system/module-system.js?_=1.1.1"
    ],
    'api_url' => env('API_URL', '')
];