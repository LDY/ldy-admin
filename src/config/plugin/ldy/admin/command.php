<?php
/*
 * @Date: 2022-09-28 22:52:03
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2022-10-23 22:07:43
 */

use Ldy\Command\LdyAdminApi;
use Ldy\Command\LdyAdminGenerateConfig;

return [
    LdyAdminApi::class,
    LdyAdminGenerateConfig::class
];