<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-06-11 16:51:32
 */

use Webman\Route;

use Ldy\Facade\Admin;

//加载模块路由文件
foreach (glob(app_path() . '/*/route/*.php') as $filename) {
    include_once($filename);
}

//禁止自动路由
Route::disableDefaultRoute();

Admin::route();





