<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-06-04 17:23:38
 * @LastEditors: LDY dong2406@126.com
 * @LastEditTime: 2022-09-12 09:35:14
 */
/**
 * This file is part of webman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link      http://www.workerman.net/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

return [
    'default' => [
        'host' => env('REDIS_HOST','127.0.0.1'),
        'password' => env('REDIS_PASSWORD','1234567'),
        'port' => env('REDIS_PORT', 6379),
        'database' => 0,
    ],
];
