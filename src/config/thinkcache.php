<?php
/*
 * @Author: LDY dong2406@126.com
 * @Date: 2022-09-12 08:32:24
 * @LastEditors: 搬铁的码农 dong2406@126.com
 * @LastEditTime: 2022-10-27 10:45:40
 */
return [
    'default' => 'redis',
    'stores' => [
        'file' => [
            'type' => 'File',
            // 缓存保存目录
            'path' => runtime_path() . '/cache/',
            // 缓存前缀
            'prefix' => '',
            // 缓存有效期 0表示永久缓存
            'expire' => 0,
        ],
        'redis' => [
            'type' => 'redis',
            'host' => env('REDIS_HOST','127.0.0.1'),
            'password'=> env('REDIS_PASSWORD','1234567'),
            'port' => 6379,
            'prefix' => 'ldy_qs_',
            'expire' => 0,
        ],
    ],
];